import React, { Component } from 'react';

//redux 
import {connect} from 'react-redux';
import {mostrarProducto, actualizarProducto} from '../actions/productosActions';

class editarProducto extends Component {

    state = {
        nombre: '',
        precio: '',
        error: false
    }
    // primer metodo que inicia 
    componentDidMount () { 
        /// console.log(this.props.match.params.id);  leeer el parametro por cabecera
        //this.props.mostrarProducto(this.props.match.params.id);
        const {id} = this.props.match.params;

        this.props.mostrarProducto(id);
    }

    /// metodo que recibe el siguiente props y estado
  
    componentWillReceiveProps(nextProps, nextState) { 
        console.log(nextProps);
        const {nombre, precio } =  nextProps.producto;

        this.setState({ 
            nombre,
            precio
        })
    }

    nombreProducto = e =>  { 
        this.setState({
            nombre: e.target.value
        })
    }

    nuevoPrecio = e => { 
        this.setState({ 
            precio: e.target.value
        })
    }

    updateProducto = e => { 
        e.preventDefault();
        
        ///validacion formulario 
        const {nombre, precio} =  this.state;

        if(nombre === '' || precio === '' ) { 
            this.setState({ 
                error: true
            })
        }else { 
            this.setState({ 
                error: false
            })           
            
        }

        //tomar el id de la ruta 
        const {id} = this.props.match.params;

        //crear el nuevo objeto para actualizar  
        const objetoProduc = { 
            id,
            nombre, 
            precio
        }

        //metodo para actualizar del cual proviene de productoActions 
        this.props.actualizarProducto(objetoProduc);

        //redireccionar al listado de productos 
        this.props.history.push('/');

    }

    render() { 
        const {error, precio, nombre} = this.state; // obtener los datos del estado pra llenar los campos 

        return( 
            <div className="row justify-content-center mt-5">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-body">
                            <h2 className="text-center">Agregar Nuevo Producto</h2>
                            <form onSubmit={this.updateProducto} >
                                <div className="form-group">
                                    <label>Titulo</label>
                                    <input type="text" defaultValue={nombre} onChange={this.nombreProducto} className="form-control" placeholder="Titulo" />
                                </div>
                                <div className="form-group">
                                    <label>Precio del Producto</label>
                                    <input type="text" defaultValue={precio} onChange={this.nuevoPrecio} className="form-control" placeholder="Precio" />
                                </div>
                                <button type="submit" className="btn btn-primary font-weight-bold text-uppercase d-block w-100">Actualizar Datos</button>
                            </form>
                            {
                                error ?
                                    <div className="font-weight-bold alert alert-danger text-center mt-4">
                                        Todosos Los campos son Obligatorios
                                </div>
                                    : ''
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

//state   mapear statod del props
const mapStateToPropsp = state => ({ 
    producto: state.productos.producto
})

export default connect(mapStateToPropsp, {mostrarProducto, actualizarProducto })  (editarProducto);