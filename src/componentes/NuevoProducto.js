import React, { Component } from 'react';

// redux 
import { connect } from 'react-redux';
import { guardarProducto } from '../actions/productosActions';

class NuevoProducto extends Component {

    /// declaraando las variables 
    state = { 
        nombre: '',
        precio: '',
        error: false
    }

    /// obteniendo los valores 
    nombreProducto = e => { 
        this.setState({ 
            nombre: e.target.value
        })
    }

    nuevoPrecio = e => { 
        this.setState({ 
            precio: e.target.value
        })
    }

    guardarNuevoProducto = e => { 
        e.preventDefault();

        // validacion  
        const {nombre, precio} = this.state;
        
        if(nombre === '' || precio === '') { 
            this.setState({ 
                error: true // validacion de mensaje                                 
            })
            alert('verifica los datos')
            
        } else {
            this.setState({
                error: false // validacion de mensaje 
            })

            // crear el nuevo objeto para guardar 
            const newProduct = {
                nombre: this.state.nombre,
                precio: this.state.precio
            }
            console.log(newProduct);

            // metodo de productoActions.js 
            this.props.guardarProducto(newProduct);

            //redireccionar una vez que se se halla guardado 
            this.props.history.push('/')
        }

    }


    render() {
        
        const {error} =  this.state;  /// se obtiene el valor de error 

        return (
            <div className="row justify-content-center mt-5">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-body">
                            <h2 className="text-center">Agregar Nuevo Producto</h2>
                            <form onSubmit={this.guardarNuevoProducto} >
                                <div className="form-group">
                                    <label>Titulo</label>
                                    <input type="text" onChange={this.nombreProducto} className="form-control" placeholder="Titulo" />
                                </div>
                                <div className="form-group">
                                    <label>Precio del Producto</label>
                                    <input type="text" onChange={this.nuevoPrecio} className="form-control" placeholder="Precio" />
                                </div>
                                <button type="submit" className="btn btn-primary font-weight-bold text-uppercase d-block w-100">Agregar</button>
                            </form>

                     {/* VALIDACION DE ERROR, PARA MOSTRAR EL MENSAJE   */}
                            {
                                error ? /// si existe ele error manda la alerta 
                                <div className="font-weight-bold alert-danger text-center mt-4 ">
                                Todos los campos son obligatorios.                                
                                </div>
                                : '' // si no, manda comillas normales y no muestres nada 

                            }

                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default connect(null, {guardarProducto})( NuevoProducto);